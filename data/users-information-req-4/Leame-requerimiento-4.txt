Para el manejo de ratings de usuarios se tienen los datos:
1. ratings_filtered.cvs (calificacion de usuarios a diferentes peliculas del festival)
2. user_request_p3.zip (requerimientos (formato json) que puede hacer un usuario dando sus ratings a algunas peliculas)

Tambien se tiene un estimado de la similitud de peliculas del festival (informacion equivalente en 2 formatos):
1. simMatriz.json (similitud de una pelicula con las demas)
2. sims.json (similitud de una pelicula con las demas en orden ascendente de su identificador)

Para el requerimiento 4, la solicitud hay que hacerla a traves de uno de las siguientes modalidades:
1. identificador del usuario (consultando los ratings que ha dado de peliculas)
2. requerimiento del usuario (dando la informacion de ratings de peliculas)

Hay que resolver el problema bajo una unica modalidad. Se puede adaptar el cliente del proyecto a la modalidad seleccionada por su grupo.
