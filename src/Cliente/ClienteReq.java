package Cliente;

import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

import API.SistemaRecomendacion;
import VOS.VOFranquicia;
import VOS.VOGeneroPelicula;
import VOS.VOTeatro;
import VOS.VOUsuario;

/**
 * Requerimientos Proyecto 3
 * @author Camilo Montenegro
 *
 */
public class ClienteReq {

	BufferedWriter escritor;
	Scanner lector;


	private SistemaRecomendacion sr;

	public ClienteReq(BufferedWriter escritor, Scanner lector) {
		this.escritor = escritor;
		this.lector = lector;
		sr = new SistemaRecomendacion();
	}
	
	public void pruebas() {
		int opcion = -1; 

		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto 3 ---------------\n");
				escritor.write("Requerimientos:\n");
				escritor.write("1: Cargar salas. (R1) \n");
				escritor.write("2: Cargar Cartelera (R2) \n");
				escritor.write("3: Cargar red. (R3) \n");
				escritor.write("4: Generar plan películas. (R4) \n");
				escritor.write("5: Generar plan películas por genero. (R5) \n");
				escritor.write("6: Generar plan películas por franquicia. (R6) \n");
				escritor.write("7: Generar plan películas por genero y desplazamiento. (R7) \n");
				escritor.write("8: Generar plan películas por genero, desplazamiento y franquicia. (R8) \n");
				escritor.write("9: Generar MST. (R9) \n");
				escritor.write("10: Generar lista de rutas posibles. (R10) \n");
				escritor.write("0: Salir\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();

				switch(opcion) {
				case 1: r1(); break;
				case 2: r2(); break;
				case 3: r3(); break;
				case 4: r4(); break;
				case 5: r5(); break;
				case 6: r6(); break;
				case 7: r7(); break;
				case 8: r8(); break;
				case 9: r9(); break;
				case 10: r10(); break;
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}

	private void r1() throws IOException{

		long tiempo = System.nanoTime();
		escritor.write("Teatros cargados:\n");
		escritor.write("\n");
		escritor.flush();
		
		sr.cargarTeatros("data/teatros_v3.json");

		tiempo = System.nanoTime() - tiempo;
		escritor.write("\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.write("\n");
		escritor.flush();
		lector.next();
	}


	private void r2() throws IOException{
		long tiempo = System.nanoTime();
		escritor.write("Peliculas cargadas:\n");
		escritor.write("\n");
		escritor.flush();

		escritor.write("\n");
		escritor.write("Dia 1:");
		escritor.write("\n");
		escritor.flush();
		sr.cargarCartelera("data/Programacion-v3/dia1.json",1);
		escritor.write("\n");
		escritor.write("Dia 2:");
		escritor.write("\n");
		escritor.flush();
		sr.cargarCartelera("data/Programacion-v3/dia2.json",2);
		escritor.write("\n");
		escritor.write("Dia 3:");
		escritor.write("\n");
		escritor.flush();
		sr.cargarCartelera("data/Programacion-v3/dia3.json",3);
		escritor.write("\n");
		escritor.write("Dia 4:");
		escritor.write("\n");
		escritor.flush();
		sr.cargarCartelera("data/Programacion-v3/dia4.json",4);
		escritor.write("\n");
		escritor.write("Dia 5:");
		escritor.write("\n");
		escritor.flush();
		sr.cargarCartelera("data/Programacion-v3/dia5.json",5);

		tiempo = System.nanoTime() - tiempo;
		escritor.write("\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r3() throws IOException
	{
		long tiempo = System.nanoTime();
		escritor.write("Tiempos cargados:\n");
		escritor.write("\n");
		escritor.flush();

		sr.cargarRed("data/tiemposIguales_v2.json");
  
		tiempo = System.nanoTime() - tiempo;
		escritor.write("\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r4() throws IOException
	{
	
		escritor.write("Ingrese la direccion del archivo JSON donde esta la info del Usuario: \n");
		escritor.flush();
		String idUsuario = lector.next();
		System.out.println(idUsuario);

		escritor.write("Ingrese la fecha: \n");
		escritor.flush();
		String fecha = lector.next();
		System.out.println(fecha);

		
	
		long tiempo = System.nanoTime();
		sr.PlanPeliculas("data/user_requests_p3/request2.json", 1);
			
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r5() throws IOException{

		escritor.write("Ingrese el ID del usuario: \n");
		escritor.flush();
		String usuarioID = lector.next();
		System.out.println(usuarioID);
		
		escritor.write("Ingrese el genéro de preferencia: \n");
		escritor.flush();
		String genero = lector.next();
		System.out.println(genero);

		long tiempo = System.nanoTime();
		sr.PlanPorGenero("Action", "data/user_requests_p3/request1.json");

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r6() throws IOException{

		escritor.write("Ingrese la fecha: \n");
		escritor.flush();
		String fecha = lector.next();
		System.out.println(fecha);
		
		escritor.write("Ingrese la franquicia: \n");
		escritor.flush();
		String franquicia = lector.next();
		System.out.println(franquicia);
		
		escritor.write("Ingrese la franja: \n");
		escritor.flush();
		String franja = lector.next();
		System.out.println(franja);
		
		
		long tiempo = System.nanoTime();

		sr.PlanPorFranquicia("Cine Colombia", 1, "am");
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r7() throws IOException{


		escritor.write("Ingrese el genero: \n");
		escritor.flush();
		String genero = lector.next();
		System.out.println(genero);

		escritor.write("Ingrese la fecha: \n");
		escritor.flush();
		String fecha = lector.next();
		System.out.println(fecha);

		
		long tiempo = System.nanoTime();

		sr.PlanPorGeneroYDesplazamiento("Action", 1);
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r8() throws IOException{

		
		escritor.write("Ingrese el genero: \n");
		escritor.flush();
		String genero = lector.next();
		System.out.println(genero);
		
		escritor.write("Ingrese la fecha: \n");
		escritor.flush();
		String fecha = lector.next();
		System.out.println(fecha);
		
		escritor.write("Ingrese la franquicia: \n");
		escritor.flush();
		String franquicia = lector.next();
		System.out.println(franquicia);

		

		long tiempo = System.nanoTime();

		sr.PlanPorGeneroDesplazamientoYFranquicia("Action", 1, "Cine Colombia");
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r9() throws NumberFormatException, IOException
	{
		
		long tiempo = System.nanoTime();

		sr.generarMapa();
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}


	private void r10() throws IOException{

		
		escritor.write("Ingrese el teatro origen: \n");
		escritor.flush();
		String teatro = lector.next();
		System.out.println(teatro);
		
		escritor.write("Ingrese el N máximo de teatros. \n");
		escritor.flush();
		int maxTeatros = lector.nextInt();
		System.out.println(maxTeatros);

	
		long tiempo = System.nanoTime();

		sr.rutasPosible("Cine Colombia Andino", 20);
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

}
