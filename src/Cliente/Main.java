package Cliente;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * Cliente Proyecto3 Festival de Cine
 * @author Camilo Montenegro
 *
 */
public class Main {
	
	static BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(System.out));
	static Scanner lector = new Scanner(System.in);
	
	public static void main(String[] args) {
		ClienteReq cliente = new ClienteReq(escritor, lector);
		cliente.pruebas();
	}
	
}