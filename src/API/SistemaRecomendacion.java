package API;

import java.io.FileReader;
import java.util.Comparator;
import java.util.Date;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


import VOS.VOFranquicia;
import VOS.VOGeneroPelicula;
import VOS.VOPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOPeliculaRating;
import VOS.VOTeatro;
import VOS.VOUbicacion;
import VOS.VOUsuario;
import estructuras.EncadenamientoSeparadoTH;
import estructuras.GrafoTeatros;
import estructuras.Lista;
import estructuras.WeightedDirectedGraph;
import logica.MergeSort;
import VOS.Edge;
import VOS.PC;

public class SistemaRecomendacion{
	
	private GrafoTeatros teatros;
	private EncadenamientoSeparadoTH<Integer, VOPelicula> diccionario;
	private Lista<Integer> idPeliculas;
	
	public SistemaRecomendacion(){
		teatros = new GrafoTeatros();
		diccionario = new EncadenamientoSeparadoTH<>(7);
		idPeliculas = new Lista<>();
		cargarDiccionario("data/movies_filtered.json");
		cargarSimilitudes("data/sims.json");
	}
	
	public int sizeTeatros(){
		return teatros.numVertices();
	}
	
	public int sizeMovies(){
		return diccionario.darTamanio();
	}
	
	private void cargarDiccionario(String ruta){
		try {
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader(ruta));
			JSONArray array = (JSONArray) obj;
			for (Object object : array) {
				JSONObject jsonObject = (JSONObject)object;
				int id;
				try{
					id = Integer.parseInt(((Long)jsonObject.get("movie_id")+"").trim());
				}
				catch (NumberFormatException e) {
					e.printStackTrace();
					continue;
				}
				VOPelicula p = new VOPelicula(id, (String)jsonObject.get("title"), new Lista<>(), new Lista<>(), new EncadenamientoSeparadoTH<>(7));
				for(String s : ((String)jsonObject.get("genres")).split("\\|")){
					p.getGeneros().add(s.trim());
				}
				diccionario.insertar(id, p);
				idPeliculas.add(id);
			}
			MergeSort.sort(idPeliculas, new Comparator<Integer>(){
				public int compare(Integer o1, Integer o2) {
					return o1-o2;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void cargarSimilitudes(String ruta){
		try {
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader(ruta));
			JSONArray array = (JSONArray) obj;
			for (Object object : array) {
				JSONObject jsonObject = (JSONObject)object;
				int id;
				try{
					id = Integer.parseInt(((Long)jsonObject.get("movieId")+"").trim());
				}
				catch (NumberFormatException e) {
					e.printStackTrace();
					continue;
				}
				int n=0;
				String[] simu = ((String)jsonObject.get("similitudes")).split(",");
				simu[0] = simu[0].substring(1);
				simu[simu.length-1] = simu[simu.length-1].substring(0, simu[simu.length-1].length()-1);
				for(String sim: simu){
					double similitud;
					if(sim.trim().equals("NaN")){
						similitud=0;	
					}
					else{	
						similitud = Double.parseDouble(sim.trim());
					}
					diccionario.darValor(id).getSimilitudes().insertar(idPeliculas.get(n), similitud);
					n++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void cargarTeatros(String ruta){
		try {
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader(ruta));
			JSONArray array = (JSONArray) obj;
			for (Object object : array) {
				JSONObject jsonObject = (JSONObject)object;
				String nombre = ((String) jsonObject.get("Nombre")).trim();
				String[] ubicacion = ((String)jsonObject.get("Ubicacion geografica (Lat|Long)")).split("\\|");
				VOUbicacion  u = new VOUbicacion(0.0, 0.0);
				try {
					u.setLatitud(Double.parseDouble(ubicacion[0]));
					u.setLongitud(Double.parseDouble(ubicacion[1]));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				VOFranquicia franquicia = new VOFranquicia((String) jsonObject.get("Franquicia"));
				VOTeatro teatro = new VOTeatro(nombre, u, franquicia);
				teatros.agregarVertice(nombre, teatro);
				System.out.println(franquicia.getNombre()+" -- "+nombre+" -- "+u.getLatitud()+"/"+u.getLongitud());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void cargarCartelera(String ruta, Integer dia){
		try {
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader(ruta));
			JSONArray array = (JSONArray) obj;
			for (Object object : array) {
				JSONObject jsonObject = (JSONObject)object;
				String nombre = ((String) jsonObject.get("teatros")).trim();
				EncadenamientoSeparadoTH<Integer, EncadenamientoSeparadoTH<Integer, VOPelicula>> v;
				try{
					v = teatros.darVertice(nombre).getCartelera();
				}catch(Exception e){
					continue;
				}
				EncadenamientoSeparadoTH<Integer, VOPelicula> pel = new EncadenamientoSeparadoTH<>(1);
				JSONObject teatro = (JSONObject) jsonObject.get("teatro");
				for (Object p : (JSONArray) teatro.get("peliculas")) {
					JSONObject o = (JSONObject) p;
					Integer id = Integer.parseInt((Long)o.get("id")+"");
					VOPelicula h = diccionario.darValor(id).clone();
					for (Object w : (JSONArray) o.get("funciones")) {
						JSONObject u = (JSONObject) w;
						String[] d = ((String)u.get("hora")).split(" ");
						String[] f = d[0].split(":");
						double hora = Double.parseDouble(f[0]);
						if((d[1].split(""))[0].equals("p")&&hora<12){
							hora+=12;
						}
						h.getHorarios().add(hora);
					}
					System.out.print(nombre+" || "+h.getNombre()+" -- ");
					for(double ho:h.getHorarios()){
						System.out.print((int)ho+":00 | ");
					}
					System.out.println();
					pel.insertar(id, h);
				}
				v.insertar(dia, pel);
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void cargarRed(String ruta){
		try {
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader(ruta));
			JSONArray array = (JSONArray) obj;
			int a = 0;
			for (Object object : array) {
				JSONObject jsonObject = (JSONObject)object;
				double tiempo;
				try{
					tiempo = (Double.parseDouble(((Long)jsonObject.get("Tiempo (minutos)")+"").trim()))/60.0;
				}
				catch (NumberFormatException e) {
					e.printStackTrace();
					continue;
				}
				String origen = (String)jsonObject.get("Teatro 1");
				String destino = (String)jsonObject.get("Teatro 2");
				teatros.agregarArco(origen, destino, tiempo);
				teatros.agregarArco(destino, origen, tiempo);
				System.out.println(origen+" <--> "+destino+" | "+tiempo+" horas");
				a+=2;
			}
			System.out.println();
			System.out.println("Reales: "+teatros.numArcos()+" Esperados: "+a);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Lista<VOPeliculaPlan> PlanPeliculas(String rutaUsuario,int fecha){
		try {
			JSONParser parser = new JSONParser();
			JSONObject obj = (JSONObject)parser.parse(new FileReader(rutaUsuario));
			JSONArray array = (JSONArray) ((JSONObject)obj.get("request")).get("ratings");
			VOUsuario u = new VOUsuario(new Lista<>(), (String)((JSONObject)obj.get("request")).get("user_name"));
			for (Object object : array) {
				JSONObject jsonObject = (JSONObject)object;
				int idPelicula;
				int rating;
				try{
					idPelicula = Integer.parseInt(((Long)jsonObject.get("item_id")+"").trim());
					rating = Integer.parseInt(((Long)jsonObject.get("rating")+"").trim());
				}
				catch (NumberFormatException e) {
					e.printStackTrace();
					continue;
				}
				u.getRatings().add(new VOPeliculaRating(idPelicula, rating));
			}
			System.out.println();
			System.out.println("Ratings de "+u.getNombre()+":");
			for(VOPeliculaRating r:u.getRatings()){
				System.out.println(diccionario.darValor(r.getIdPelicula()).getNombre()+": "+r.getRating());
			}
			System.out.println();
			System.out.println("Recomendadas: ");
			Lista<Integer> reco = new Lista<>();
			for(VOPeliculaRating r:recomendar(u)){
				System.out.print(diccionario.darValor(r.getIdPelicula()).getNombre()+"; ");
				reco.add(r.getIdPelicula());
			}
			System.out.println();
	
			for(VOPeliculaPlan pp: teatros.req4(reco, 1)){
				System.out.println(pp.getTeatro()+": "+diccionario.darValor(pp.getPelicula()).getNombre()+" | "+pp.getHoraInicio()+" --> "+pp.getHoraFin()+" Distancia anterior: "+pp.getDistancia());
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public Lista<VOPeliculaPlan> PlanPorGenero(String genero, String rutaUsuario){
		try {
			JSONParser parser = new JSONParser();
			JSONObject obj = (JSONObject)parser.parse(new FileReader(rutaUsuario));
			JSONArray array = (JSONArray) ((JSONObject)obj.get("request")).get("ratings");
			VOUsuario u = new VOUsuario(new Lista<>(), (String)((JSONObject)obj.get("request")).get("user_name"));
			for (Object object : array) {
				JSONObject jsonObject = (JSONObject)object;
				int idPelicula;
				int rating;
				try{
					idPelicula = Integer.parseInt(((Long)jsonObject.get("item_id")+"").trim());
					rating = Integer.parseInt(((Long)jsonObject.get("rating")+"").trim());
				}
				catch (NumberFormatException e) {
					e.printStackTrace();
					continue;
				}
				u.getRatings().add(new VOPeliculaRating(idPelicula, rating));
			}
			System.out.println();
			System.out.println("Ratings de "+u.getNombre()+":");
			for(VOPeliculaRating r:u.getRatings()){
				System.out.println(diccionario.darValor(r.getIdPelicula()).getNombre()+": "+r.getRating());
			}
			System.out.println();
			System.out.println("Recomendadas y del genero "+genero+": ");
			Lista<Integer> reco = new Lista<>();
			for(VOPeliculaRating r:recomendarGenero(u, genero)){
					System.out.print(diccionario.darValor(r.getIdPelicula()).getNombre()+"; ");
					reco.add(r.getIdPelicula());
			}
			System.out.println();
			Lista<VOPeliculaPlan> r = new Lista<>();
			int dia =0;
			for (int i = 1; i < 6; i++) {
				Lista<VOPeliculaPlan> b = teatros.req4(reco.clone(), i);
				if(b.size()>r.size()){
					r=b;
					dia =i;
				}	
			}
			System.out.println("Dia: "+dia);
			for(VOPeliculaPlan pp: r){
				System.out.println(pp.getTeatro()+": "+diccionario.darValor(pp.getPelicula()).getNombre()+" | "+pp.getHoraInicio()+" --> "+pp.getHoraFin()+" Distancia anterior: "+pp.getDistancia());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public Lista<VOPeliculaPlan>  PlanPorFranquicia(String franquicia, int fecha,String franja){
		
			Lista<Integer> reco = new Lista<>();
			for(VOPelicula r:diccionario.valores()){
					reco.add(r.getId());
			}
			System.out.println();
			Lista<VOPeliculaPlan> r = teatros.req6(reco, fecha, franquicia, franja);
			for(VOPeliculaPlan pp: r){
				System.out.println(pp.getTeatro()+": "+diccionario.darValor(pp.getPelicula()).getNombre()+" | "+pp.getHoraInicio()+" --> "+pp.getHoraFin()+" Distancia anterior: "+pp.getDistancia());
			}
		
		return null;
	}


	public Lista<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(String genero, int fecha){

		Lista<Integer> reco = new Lista<>();
		for(VOPelicula r:diccionario.valores()){
			if(r.getGeneros().indexOf(genero)>-1){
				reco.add(r.getId());
			}
		}
		System.out.println();
		Lista<VOPeliculaPlan> r = teatros.req4(reco, fecha);
		for(VOPeliculaPlan pp: r){
			System.out.println(pp.getTeatro()+": "+diccionario.darValor(pp.getPelicula()).getNombre()+" | "+pp.getHoraInicio()+" --> "+pp.getHoraFin()+" Distancia anterior: "+pp.getDistancia());
		}

		return null;
	}


	public Lista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(String genero, int fecha, String franquicia){
		Lista<Integer> reco = new Lista<>();
		for(VOPelicula r:diccionario.valores()){
			if(r.getGeneros().indexOf(genero)>-1){
				reco.add(r.getId());
			}
		}
		System.out.println();
		Lista<VOPeliculaPlan> r = teatros.req6(reco, fecha,franquicia,"");
		for(VOPeliculaPlan pp: r){
			System.out.println(pp.getTeatro()+": "+diccionario.darValor(pp.getPelicula()).getNombre()+" | "+pp.getHoraInicio()+" --> "+pp.getHoraFin()+" Distancia anterior: "+pp.getDistancia());
		}

		return null;
	}

	public Lista<Edge> generarMapa(){
		for (Edge integer : teatros.req9()) {
			System.out.println(integer.getSalida()+" <-- "+integer.getPeso()+" --> "+integer.getLlegada());
		}
		return null;
	}

	public Lista<Lista<VOTeatro>> rutasPosible(String origen, int n){

		for(Lista<String> a:teatros.req10(origen, n)){
			for (String string : a) {
				System.out.print(string+" -- ");
			}
			System.out.println();
		}
		return null;
	}

	private Lista<VOPeliculaRating> recomendar(VOUsuario u){
		Lista<VOPeliculaRating> recomendaciones = new Lista<>();
		Lista<VOPeliculaRating> m = new Lista<>();
		for (VOPelicula pe : diccionario.valores()) {
			double arriba = 0.0;
			double abajo = 0.0;
			for(VOPeliculaRating r: u.getRatings()){
				double sim = pe.getSimilitudes().darValor(r.getIdPelicula());
				arriba += (sim*r.getRating());
				abajo += sim;
			}
			recomendaciones.add(new VOPeliculaRating(pe.getId(), (arriba/abajo)));
		}
		MergeSort.sort(recomendaciones, new Comparator<VOPeliculaRating>() {
			public int compare(VOPeliculaRating o1, VOPeliculaRating o2) {
				if(o1.getRating()>o2.getRating()){
					return -1;
				}
				if(o1.getRating()<o2.getRating()){
					return 1;
				}
				return 0;
			}
		});
		for(int i=0; i<20;i++){
			m.add(recomendaciones.get(i));
		}
		return m;
	}

	private Lista<VOPeliculaRating> recomendarGenero(VOUsuario u, String genero){
		Lista<VOPeliculaRating> recomendaciones = new Lista<>();
		Lista<VOPeliculaRating> m = new Lista<>();
		for (VOPelicula pe : diccionario.valores()) {
			double arriba = 0.0;
			double abajo = 0.0;
			for(VOPeliculaRating r: u.getRatings()){
				double sim = pe.getSimilitudes().darValor(r.getIdPelicula());
				arriba += (sim*r.getRating());
				abajo += sim;
			}
			recomendaciones.add(new VOPeliculaRating(pe.getId(), (arriba/abajo)));
		}
		MergeSort.sort(recomendaciones, new Comparator<VOPeliculaRating>() {
			public int compare(VOPeliculaRating o1, VOPeliculaRating o2) {
				if(o1.getRating()>o2.getRating()){
					return -1;
				}
				if(o1.getRating()<o2.getRating()){
					return 1;
				}
				return 0;
			}
		});
		for(int i=0; i<recomendaciones.size();i++){
			if(diccionario.darValor(recomendaciones.get(i).getIdPelicula()).getGeneros().indexOf(genero)>-1){
				m.add(recomendaciones.get(i));
			}
		}
		return m;
	}
}
