package API;

import estructuras.EncadenamientoSeparadoTH;
import estructuras.GrafoTeatros;
import estructuras.Lista;
import estructuras.NodoCamino;
import VOS.VOPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOTeatro;

public class Prueba {
	private static GrafoTeatros teatros = new GrafoTeatros();
	public static void main(String[] args){
		for (int i = 1; i < 5; i++) {
			VOTeatro t = new VOTeatro(i+"", null,null);
			EncadenamientoSeparadoTH<Integer, VOPelicula> pel = new EncadenamientoSeparadoTH<>(1);
			pel.insertar(i, new VOPelicula(i, i+"", new Lista<>(), null, null));
			pel.darValor(i).getHorarios().add((double)i*3.0);
			t.getCartelera().insertar(1, pel);
			teatros.agregarVertice(i+"", t);
		}
		teatros.agregarArco("1", "2", 0.5);
		teatros.agregarArco("2", "3", 0.5);
		teatros.agregarArco("3", "4", 0.5);
		teatros.agregarArco("4", "3", 0.5);
		teatros.agregarArco("3", "2", 0.5);
		teatros.agregarArco("2", "1", 0.5);
		teatros.agregarArco("4", "2", 0.5);
		teatros.agregarArco("2", "4", 0.5);
		teatros.agregarArco("1", "4", 0.5);
		teatros.agregarArco("4", "1", 0.5);
		teatros.agregarArco("1", "3", 0.5);
		teatros.agregarArco("3", "1", 0.5);
		Lista<Integer> reco = new Lista<>();
		reco.add(1);
		reco.add(2);
		reco.add(3);
		reco.add(4);
		for(VOPeliculaPlan a:teatros.DFS1("2", reco, 1)){
			System.out.print(a.getTeatro()+", ");
		}
	}
	

}
