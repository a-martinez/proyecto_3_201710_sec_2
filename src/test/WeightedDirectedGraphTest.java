package test;

import java.util.Comparator;

import estructuras.Lista;
import estructuras.NodoCamino;
import estructuras.WeightedDirectedGraph;
import junit.framework.TestCase;
import logica.MergeSort;

public class WeightedDirectedGraphTest extends TestCase {
	
	private WeightedDirectedGraph<Integer, Integer> grafo;
	
	private Integer[] dfs1vertices = {1,5,7,9,2,4,8,3,6};
	private Double[] dfs1pesos = {0.0,4.0,9.0,15.0,10.0,11.0,14.0,18.0,9.0};
	private Integer[] dfs1arcos = {0,1,2,3,3,4,5,6,2};
	
	private Integer[] bfs1vertices = {1,5,7,2,4,6,9,8,3};
	private Double[] bfs1pesos = {0.0,4.0,9.0,12.0,12.0,9.0,15.0,15.0,15.0};
	private Integer[] bfs1arcos = {0,1,2,2,2,2,3,3,3};
	
	private Integer[] grados = {1,2,2,1,4,3,3,1,1};
	
	private Integer[] caminoBFS = {1,5,6,3};
	private Integer[] caminoDFS = {1,5,7,2,4,8,3};
	
	private Integer[] caminoCorto1 = {1};
	private Integer[] caminoCorto2 = {1,5,7,2};
	private Integer[] caminoCorto3 = {1,5,6,3};
	private Integer[] caminoCorto4 = {1,5,7,2,4};
	private Integer[] caminoCorto5 = {1,5};
	private Integer[] caminoCorto6 = {1,5,6};
	private Integer[] caminoCorto7 = {1,5,7};
	private Integer[] caminoCorto8 = {1,5,7,2,4,8};
	private Integer[] caminoCorto9 = {1,5,7,2,9};
	
	
	protected void setUp1() {
		grafo = new WeightedDirectedGraph<>();
		for (int i = 1; i < 10; i++) {
			grafo.agregarVertice(i, i);
		}
		grafo.agregarArco(1, 5, 4);
		grafo.agregarArco(2, 9, 4);
		grafo.agregarArco(2, 4, 1);
		grafo.agregarArco(3, 1, 2);
		grafo.agregarArco(3, 5, 4);
		grafo.agregarArco(4, 8, 3);
		grafo.agregarArco(5, 7, 5);
		grafo.agregarArco(5, 2, 8);
		grafo.agregarArco(5, 4, 8);
		grafo.agregarArco(5, 6, 5);
		grafo.agregarArco(6, 4, 4);
		grafo.agregarArco(6, 8, 6);
		grafo.agregarArco(6, 3, 6);
		grafo.agregarArco(7, 9, 6);
		grafo.agregarArco(7, 2, 1);
		grafo.agregarArco(7, 1, 10);
		grafo.agregarArco(8, 3, 4);
		grafo.agregarArco(9, 1, 4);
	}
	
	public void testDarVertice(){
		setUp1();
		for (int i = 1; i < 10; i++) {
			assertEquals(i, (int)grafo.darVertice(i));
		}
	}
	
	public void testDFS(){
		setUp1();
		int i = 0;
		for(NodoCamino<Integer> nc: grafo.DFS(1)){
			assertEquals(dfs1vertices[i], nc.getVertice());
			assertEquals(dfs1pesos[i], nc.getPeso());
			assertEquals((int)dfs1arcos[i], (int)nc.getArcos());
			i++;
		}
	}
	
	public void testBFS(){
		setUp1();
		int i = 0;
		for(NodoCamino<Integer> nc: grafo.BFS(1)){
			assertEquals(bfs1vertices[i], nc.getVertice());
			assertEquals(bfs1pesos[i], nc.getPeso());
			assertEquals((int)bfs1arcos[i], (int)nc.getArcos());
			i++;
		}
	}
	
	public void testCaminoBFS(){
		setUp1();
		int i = 0;
		for(NodoCamino<Integer> nc: grafo.darCaminoBFS(1, 3)){
			assertEquals(caminoBFS[i], nc.getVertice());
			i++;
		}
	}
	
	public void testCaminoDFS(){
		setUp1();
		int i = 0;
		for(NodoCamino<Integer> nc: grafo.darCaminoDFS(1, 3)){
			assertEquals(caminoDFS[i], nc.getVertice());
			i++;
		}
	}
	
	public void testDarGrado(){
		setUp1();
		for (int i = 0; i < 9; i++) {
			assertEquals((int)grados[i], (int)grafo.darGrado(i+1));
		}
	}
	
	public void testNumArcos(){
		setUp1();
		assertEquals(18, grafo.numArcos());
	}
	
	public void testNumVertices(){
		setUp1();
		assertEquals(9, grafo.numVertices());
	}
	
	public void testCaminosMasCortos(){
		setUp1();
		Lista<Lista<Integer>> l = grafo.caminosCortosDesde(1);
		MergeSort.sort(l, new Comparator<Lista<Integer>>() {
			public int compare(Lista<Integer> o1, Lista<Integer> o2) {
				return o1.get(0)-o2.get(0);
			}
		});
		Lista<Integer[]> expected = new Lista<>();
		expected.add(caminoCorto1);
		expected.add(caminoCorto2);
		expected.add(caminoCorto3);
		expected.add(caminoCorto4);
		expected.add(caminoCorto5);
		expected.add(caminoCorto6);
		expected.add(caminoCorto7);
		expected.add(caminoCorto8);
		expected.add(caminoCorto9);
		for (int k = 0; k < expected.size(); k++) {
			for (int i = 0; i < expected.get(k).length; i++) {
				assertEquals(expected.get(k)[i], l.get(k).get((expected.get(k).length-1)-i));
			}
		}
	}
}
