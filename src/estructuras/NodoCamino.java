package estructuras;

public class NodoCamino<K>{
	private	K vertice;
	private	K anterior;
	private	double peso;
	private	int arcos;
	public K getVertice() {
		return vertice;
	}
	public void setVertice(K vertice) {
		this.vertice = vertice;
	}
	public K getAnterior() {
		return anterior;
	}
	public void setAnterior(K anterior) {
		this.anterior = anterior;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public int getArcos() {
		return arcos;
	}
	public void setArcos(int arcos) {
		this.arcos = arcos;
	}
	public NodoCamino(K vertice, K anterior, double peso, int arcos) {
		this.vertice = vertice;
		this.anterior = anterior;
		this.peso = peso;
		this.arcos = arcos;
	}
	
}
