package estructuras;

import java.io.Serializable;
import java.util.Iterator;

public class Lista<T> implements Iterable<T>, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6L;
	private int size = 0;
	
	public Lista() {}
	
	private class Nodo<T> implements Serializable
	{
		private Nodo<T> siguiente;
		private Nodo<T> anterior;
		private T elemento;

		public Nodo(T e)
		{
			elemento = e;
		}
		public T darElemento()
		{
			return elemento;
		}
		public Nodo<T> darSiguiente()
		{
			return siguiente;
		}
		public void cambiarSiguiente(Nodo<T> a){
			siguiente = a;
		}
		public Nodo<T> darAnterior()
		{
			return anterior;
		}
		public void cambiarAnterior(Nodo<T> a){
			anterior = a;
		}
		public void cambiarElemento(T a){
			elemento = a;
		}
	}

	private Nodo<T> primero;
	private Nodo<T> ultimo;
	
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() {
			Nodo<T> a = primero;
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				if(a==null){
					return false;
				}
				return true;
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				T b = (T) a.darElemento();
				a=a.darSiguiente();
				return b;
			}

		};
	}

	public synchronized void add(T elem)
	{
		if(primero==null){
			primero = new Nodo<T>(elem);
			ultimo=primero;
			size++;
		}
		else{
			Nodo<T> a = new Nodo<T>(elem);
			ultimo.cambiarSiguiente(a);
			a.cambiarAnterior(ultimo);
			ultimo = a;
			size++;
		}
	}
	
	public void set(int pos, T elem){
		if(pos<size){
			Nodo<T> a = primero;
			for(;pos>0;pos--){
				a = a.darSiguiente();
			}
			a.cambiarElemento(elem);
		}
		else{
			throw new  IllegalStateException();
		}
	}
	
	public void add(int pos, T elem){
		if(pos<size){
			Nodo<T> a = primero;
			Nodo<T> b = new Nodo<T>(elem);
			for(;pos>0;pos--){
				a = a.darSiguiente();
			}
			if(a.darAnterior()==null){
				a.cambiarAnterior(b);
				b.cambiarSiguiente(a);
				primero = b;
				return;
			}
			if(a.darSiguiente()==null){
				a.cambiarSiguiente(b);
				b.cambiarAnterior(a);
				ultimo = b;
				return;
			}
			a.darAnterior().cambiarSiguiente(b);
			b.cambiarAnterior(a.darAnterior());
			a.cambiarAnterior(b);
			b.cambiarSiguiente(a);
			
		}
		else{
			throw new  IllegalStateException();
		}
	}
	
	
	
	public T get(int pos){
		Nodo<T> a = primero;
		while(a!=null && pos>0){
			 pos--;
			 a = a.darSiguiente();
		}
		if(a==null||pos<0){
			throw new IllegalStateException();
		}
		else{
			return a.darElemento();
		}
	}
	
	public int size() {
		return size;
	}

	public boolean isEmpty(){
		if(primero==null){
			return true;
		}else{
			return false;
		}
	}
	
	public Lista<T> clone(){
		Lista<T> a = new Lista<>();
		Nodo<T> b = primero;	
		while(b!=null){
			a.add(b.darElemento());
			b=b.darSiguiente();
		}
		return a;
	}

	public T remove(int pos){
		Nodo<T> a = primero;
		while(a!=null&&pos>0){
			a = a.darSiguiente();
			pos--;
		}

		if(a!=null&&pos==0){
			size--;
			if(ultimo==primero){
				ultimo=null;
				primero=null;
				return a.darElemento();
			}
			if(ultimo==a){
				ultimo = a.darAnterior();
				ultimo.cambiarSiguiente(null);
				return a.darElemento();
			}
			if(primero==a){
				primero = a.darSiguiente();
				primero.cambiarAnterior(null);
				return a.darElemento();
			}
			a.darAnterior().cambiarSiguiente(a.darSiguiente());
			a.darSiguiente().cambiarAnterior(a.darAnterior());
			return a.darElemento();
		}
		return null;
	}
	
	public int indexOf(T elem){
		int t=0;
		Nodo<T> a = primero;
		while(a!=null){
			if(a.darElemento().equals(elem)){
				return t;
			}
			 a = a.darSiguiente();
			 t++;
		}
		return -1;
	}
}
