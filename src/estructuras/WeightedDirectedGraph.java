package estructuras;
import java.util.Iterator;
import java.util.NoSuchElementException;

import estructuras.Lista;

public class WeightedDirectedGraph<K,V> {
	
	class Arco{
		K salida;
		K llegada;
		double peso;
		public Arco(K salida, K llegada, double peso) {
			this.salida = salida;
			this.llegada = llegada;
			this.peso = peso;
		}
		
	}
	
	class Vertice{
		K llave;
		V valor;
		EncadenamientoSeparadoTH<K, Arco>  relaciones;
		boolean marca;
		
		public Vertice(K llave, V valor, EncadenamientoSeparadoTH<K, WeightedDirectedGraph<K, V>.Arco> relaciones) {
			this.llave = llave;
			this.valor = valor;
			this.relaciones = relaciones;
			marca = false;
		}

		public void dfs(Lista<NodoCamino<K>> keys, K padre, double peso, int arcos){		
			if(!marca){
				marca=true;
				keys.add(new NodoCamino<K>(llave, padre, peso, arcos));	
				for (Arco a : relaciones.valores()) {
					vertices.darValor(a.llegada).dfs(keys, llave, peso+a.peso,arcos+1);
				}					
			}
		}
	}
	
	EncadenamientoSeparadoTH<K, Vertice> vertices;
	Lista<Arco> arcos;
	
	public WeightedDirectedGraph() {
		vertices = new EncadenamientoSeparadoTH<>(5);
		arcos = new Lista<>();
	}
	
	public int numVertices(){
		return vertices.darTamanio();
	}
	
	public int numArcos(){
		return arcos.size();
	}
	
	public V darVertice(K id){
		return vertices.darValor(id).valor;
	}
	
	public void agregarVertice(K id, V val){
		vertices.insertar(id, new Vertice(id, val, new EncadenamientoSeparadoTH<>(1)));
	}
	
	public void agregarArco(K idOrigen, K idDestino, double peso){
		if(vertices.tieneLlave(idOrigen)&&vertices.tieneLlave(idDestino)){
			Arco a = new Arco(idOrigen, idDestino, peso);
			vertices.darValor(idOrigen).relaciones.insertar(idDestino, a);
			arcos.add(a);
		}
	}
	
	public double darPesoArco(K idOrigen, K idDestino){
		for (Arco arco : arcos) {
			if(arco.salida==idOrigen&&arco.salida==idDestino){
				return arco.peso;
			}
		}
		throw new NoSuchElementException();
	}
	
	public Iterator<K> darVertices(){
		return vertices.llaves().iterator();
	}
	
	public int darGrado(K id){
		if(vertices.tieneLlave(id)){
			return vertices.darValor(id).relaciones.darTamanio();
		}
		throw new NoSuchElementException();
	}
	
	public Iterator<K> darVerticesAdyacentes(K id){
		if(vertices.tieneLlave(id)){
			vertices.darValor(id).relaciones.llaves().iterator();
		}
		throw new NoSuchElementException();
	}
	
	public void desmarcar(){
		for (Vertice ver : vertices.valores()) {
			ver.marca=false;
		}
	}

	public NodoCamino<K>[] DFS(K idOrigen){
		if(vertices.tieneLlave(idOrigen)){
			Lista<NodoCamino<K>> keys = new Lista<>();
			vertices.darValor(idOrigen).dfs(keys, null, 0, 0);
			NodoCamino<K>[] dfs = (NodoCamino<K>[]) new NodoCamino[keys.size()];
			for (int i = 0; i < dfs.length; i++) {
				dfs[i]=keys.get(i);
			}
			desmarcar();
			return dfs;
		}
		throw new NoSuchElementException();
	}

	public NodoCamino<K>[] BFS(K idOrigen){
		if(vertices.tieneLlave(idOrigen)){
			Lista<NodoCamino<K>> keys = new Lista<>();
			Lista<NodoCamino<K>> i = new Lista<>();
			i.add(new NodoCamino<K>(idOrigen, null, 0, 0));
			while(!i.isEmpty()){
				NodoCamino<K> a = i.remove(0);
				Vertice b = vertices.darValor(a.getVertice());
				keys.add(a);
				b.marca = true;
				for(Arco k: b.relaciones.valores()){
					if(!vertices.darValor(k.llegada).marca){
						i.add(new NodoCamino<K>(k.llegada, b.llave, a.getPeso()+k.peso, a.getArcos()+1));
						vertices.darValor(k.llegada).marca=true;
					}
				}
			}
			NodoCamino<K>[] dfs = (NodoCamino<K>[]) new NodoCamino[keys.size()];
			for (int m = 0; m < dfs.length; m++) {
				dfs[m]=keys.get(m);
			}
			desmarcar();
			return dfs;
		}
		throw new NoSuchElementException();
	}
	
	public NodoCamino<K>[] darCaminoDFS(K idOrigen, K idDestino){
		NodoCamino<K>[] a = DFS(idOrigen);
		Lista<NodoCamino<K>> b = new Lista<>();
		for (NodoCamino<K> nodoCamino : a) {
			if(nodoCamino.getVertice().equals(idDestino)){
				b.add(nodoCamino);
				K padre = nodoCamino.getAnterior();
				while(padre!=null){
					for (NodoCamino<K> nodoCamino1 : a) {
						if (nodoCamino1.getVertice().equals(padre)) {
							b.add(nodoCamino1);
							padre=nodoCamino1.getAnterior();
							break;
						}
					}
				}
				NodoCamino<K>[] dfs = (NodoCamino<K>[]) new NodoCamino[b.size()];
				for (int m = 0; m < dfs.length; m++) {
					dfs[m]=b.get((b.size()-1)-m);
				}
				return dfs;
			}
		}
		return null;
	}

	public NodoCamino<K>[] darCaminoBFS(K idOrigen, K idDestino){
		NodoCamino<K>[] a = BFS(idOrigen);
		Lista<NodoCamino<K>> b = new Lista<>();
		for (NodoCamino<K> nodoCamino : a) {
			if(nodoCamino.getVertice().equals(idDestino)){
				b.add(nodoCamino);
				K padre = nodoCamino.getAnterior();
				while(padre!=null){
					for (NodoCamino<K> nodoCamino1 : a) {
						if (nodoCamino1.getVertice().equals(padre)) {
							b.add(nodoCamino1);
							padre=nodoCamino1.getAnterior();
							break;
						}
					}
				}
				NodoCamino<K>[] dfs = (NodoCamino<K>[]) new NodoCamino[b.size()];
				for (int m = 0; m < dfs.length; m++) {
					dfs[m]=b.get((b.size()-1)-m);
				}
				return dfs;
			}
		}
		return null;
	}
	
	public Lista<Lista<K>> caminosCortosDesde(K key){
		class PC implements Comparable<PC>{
			K llave;
			Double peso;
			K anterior;
			public PC(Double peso, K anterior, K llave) {
				this.peso = peso;
				this.anterior = anterior;
				this.llave = llave;
			}
			public int compareTo(PC o) {
				return (-1)*peso.compareTo(o.peso);
			}	
		}
		EncadenamientoSeparadoTH<K, PC> s = new EncadenamientoSeparadoTH<>(1);
		MaxHeapCP<PC> heap = new MaxHeapCP<>();
		Lista<Lista<K>> respuesta = new Lista<>();
		if(vertices.tieneLlave(key)){
			heap.add(new PC(0.0, null, key));
			while(!heap.isEmpty()){
				PC a = heap.max();
				s.insertar(a.llave, a);
				Vertice b = vertices.darValor(a.llave);
				b.marca = true;
				for(Arco c: b.relaciones.valores()){
					boolean t = true;
					MaxHeapCP<PC> heapAux = new MaxHeapCP<>();
					if(!vertices.darValor(c.llegada).marca){
						while(!heap.isEmpty()){
							PC d = heap.max();
							if(c.llegada==d.llave){
								if(d.peso>(a.peso+c.peso)){
									d.peso=(a.peso+c.peso);
									d.anterior=a.llave;
								}
								t = false;
							}
							heapAux.add(d);
						}
						heap = heapAux;
						if(t){
							heap.add(new PC((a.peso+c.peso), a.llave, c.llegada));
						}
					}
				}	
				Lista<K> res = new Lista<>();
				PC n = a;
				while(n!=null){
					res.add(n.llave);
					if(n.anterior!=null){
						n = s.darValor(n.anterior);
					}
					else{
						break;
					}
				}
				respuesta.add(res);
			}
			desmarcar();
			return respuesta;
		}
		throw new NoSuchElementException();
	}

}
