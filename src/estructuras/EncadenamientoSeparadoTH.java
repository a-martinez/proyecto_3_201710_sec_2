package estructuras;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Iterator;

public class EncadenamientoSeparadoTH<K, V> implements Serializable{
	
	private static final long serialVersionUID = 5L;

	private class KV implements Serializable{
		private K llave;
		private V valor;
		public K getLlave() {
			return llave;
		}
		public void setLlave(K llave) {
			this.llave = llave;
		}
		public V getValor() {
			return valor;
		}
		public void setValor(V valor) {
			this.valor = valor;
		}
		public KV(K llave, V valor) {
			this.llave = llave;
			this.valor = valor;
		}
	}
	
	private Lista<KV>[] TH;
	
	public EncadenamientoSeparadoTH(int m) {
		TH = (Lista<KV>[]) new Lista[m];
		for (int i = 0; i < TH.length; i++) {
			TH[i] = new Lista<>();
		}
	}
	
	public int darTamanio(){
		int a =0;
		for (Lista<KV> listaLlaveValorSecuencial : TH) {
			a+=listaLlaveValorSecuencial.size();
		}
		return a;
	}
	
	public boolean estaVacia(){
		return (darTamanio()==0)?true:false;
	}
	
	private int hash(K llave){
		int hc = llave.hashCode();
		hc = Math.abs(hc);
		return hc%TH.length;
	}
	
	public int[] darLongitudListas(){
		int[] l = new int[TH.length];
		for (int i = 0; i < l.length; i++) {
			l[i]=TH[i].size();
		}
		return l;
	}
	
	private boolean reHash(){
		Lista<KV> a = new Lista<>();
		if(((double)darTamanio()/(double)TH.length)>7){
			for (Lista<KV> l : TH) {
				for (KV kv : l) {
					a.add(kv);
				}
			}
			TH = (Lista<KV>[]) new Lista[(new BigInteger(String.valueOf(TH.length*2)).nextProbablePrime()).intValue()];
			for (int i = 0; i < TH.length; i++) {
				TH[i] = new Lista<>();
			}
			for (KV kv : a) {
				insertar(kv);
			}
			return true;
		}
		return false;
	}
	
	private void insertar(KV kv){
		TH[hash((K) kv.getLlave())].add(kv);
	}
	
	public synchronized void insertar(K llave, V valor){
		if(valor==null&&tieneLlave(llave)){
			int o = 0;
			for(KV i:TH[hash(llave)]){
				if(i.getLlave().equals(llave)){
					break;
				}
				o++;
			}
			TH[hash(llave)].remove(o);		
		}
		else if(tieneLlave(llave)){
			for(KV i:TH[hash(llave)]){
				if(i.getLlave().equals(llave)){
					i.setValor(valor);
					break;
				}
			}
		}
		else{
		reHash();
		TH[hash(llave)].add(new KV(llave, valor));
		}
	}

	public boolean tieneLlave(K llave){
		for (KV kv : TH[hash(llave)]) {
			if(kv.getLlave().equals(llave)){
				return true;
			}
		}
		return false;
	}
	
	public V darValor(K llave){
		for(KV i:TH[hash(llave)]){
			if(i.getLlave().equals(llave)){
				return i.getValor();
			}
		}
		throw new  IllegalStateException();
	}
	
	public Iterable<K> llaves(){
		return new Iterable<K>(){

			@Override
			public Iterator<K> iterator() {
				// TODO Auto-generated method stub
				Lista<K> elementos = new Lista<>();
				for (Lista<KV> l : TH){
					for(KV o:l){
						elementos.add(o.getLlave());
					}
				}
				return elementos.iterator();
			}
			
		};
	}
	
	public Iterable<V> valores(){
		return new Iterable<V>(){

			@Override
			public Iterator<V> iterator() {
				// TODO Auto-generated method stub
				Lista<V> elementos = new Lista<>();
				for (Lista<KV> l : TH){
					for(KV o:l){
						elementos.add(o.getValor());
					}
				}
				return elementos.iterator();
			}
			
		};
	}
	
}
