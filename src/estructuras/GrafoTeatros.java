package estructuras;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import VOS.VOPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOPeliculaRating;
import VOS.VOTeatro;
import VOS.VOUsuario;
import estructuras.WeightedDirectedGraph.Arco;
import estructuras.WeightedDirectedGraph.Vertice;
import logica.MergeSort;
import sun.launcher.resources.launcher;
import VOS.Edge;
import VOS.PC;

public class GrafoTeatros {

	public Lista<PC> peliculasGeneroDia (int dia, Lista<Integer> reco){
		Lista<Lista<PC>> resp = new Lista<>();
		for(Vertice key: vertices.valores()){
			for(VOPelicula p:key.valor.getCartelera().darValor(dia).valores()){
				Lista<Integer> peNoVistas = reco.clone();
				if(peNoVistas.indexOf(p.getId())!=-1){
					PC n = null;
					EncadenamientoSeparadoTH<String, PC> s = new EncadenamientoSeparadoTH<>(1);
					MaxHeapCP<PC> heap = new MaxHeapCP<>();
					heap.add(new PC(p.getHorarios().get(0)+2, null, key.llave, p.getHorarios().get(0), p.getId()));
					while(!heap.isEmpty()){
						PC a = heap.max();
						if(peNoVistas.remove(peNoVistas.indexOf(a.idPelicula))!=null){
							s.insertar(a.llave, a);
							Vertice b = vertices.darValor(a.llave);
							b.marca = true;
							for(Arco c: b.relaciones.valores()){
								double distancia = c.peso;
								if(a.peso>=8&&a.peso<12){
									distancia = distancia+(distancia*0.15);
								}
								else if(a.peso>=12&&a.peso<14){
									distancia = distancia+(distancia*0.20);
								}
								boolean t = true;
								MaxHeapCP<PC> heapAux = new MaxHeapCP<>();
								Vertice llegadaArco = vertices.darValor(c.llegada);
								if(!llegadaArco.marca){
									Lista<VOPeliculaRating> menor = new Lista<>();
									for(Integer i: peNoVistas){
										try{
											for(double h:llegadaArco.valor.getCartelera().darValor(dia).darValor(i).getHorarios()){
												if(h>=(a.peso+distancia)){
													menor.add(new VOPeliculaRating(i, h));
												}
											}	
										}catch(Exception e){
											continue;
										}
									}
									if(!menor.isEmpty()){
										MergeSort.sort(menor, new Comparator<VOPeliculaRating>() {
											public int compare(VOPeliculaRating o1, VOPeliculaRating o2) {
												Double a1 = o1.getRating();
												Double a2 = o2.getRating();
												return a1.compareTo(a2);
											}
										});
										VOPeliculaRating funcion = menor.get(0);

										while(!heap.isEmpty()){
											PC d = heap.max();
											if(c.llegada==d.llave){
												if(d.peso>(funcion.getRating()+2)||peNoVistas.indexOf(d.idPelicula)==-1){
													d.peso=(funcion.getRating()+2);
													d.anterior=a.llave;
													d.idPelicula=funcion.getIdPelicula();
													d.inicio=funcion.getRating();
												}
												t = false;
											}
											heapAux.add(d);
										}
										heap = heapAux;
										if(t){
											heap.add(new PC(funcion.getRating()+2, a.llave, c.llegada, funcion.getRating(), funcion.getIdPelicula()));
										}
									}
								}
							}
							Lista<PC> res = new Lista<>();
							n=a;
							while(n!=null){
								res.add(n);
								if(n.anterior!=null){
									n = s.darValor(n.anterior);
								}
								else{
									break;
								}
							}
							resp.add(res);
						}
						else{
							boolean t=false;
							Vertice ar = vertices.darValor(a.llave);
							Lista<PC> aux = new Lista<>();
							re:while(!heap.isEmpty()){
								PC f = heap.max();
								aux.add(f);
								for(Arco da: vertices.darValor(f.llave).relaciones.valores()){
									if(da.llegada==a.llave){
										double distancia = da.peso;
										if(f.peso>=8&&f.peso<12){
											distancia = distancia+(distancia*0.15);
										}
										else if(f.peso>=12&&f.peso<14){
											distancia = distancia+(distancia*0.20);
										}
										Lista<VOPeliculaRating> menor = new Lista<>();
										for(Integer i: peNoVistas){
											try{
												for(double h:ar.valor.getCartelera().darValor(dia).darValor(i).getHorarios()){
													if(h>=(f.peso+distancia)){
														menor.add(new VOPeliculaRating(i, h));
													}
												}	
											}catch(Exception e){
												continue;
											}
										}
										if(!menor.isEmpty()){
											MergeSort.sort(menor, new Comparator<VOPeliculaRating>() {
												public int compare(VOPeliculaRating o1, VOPeliculaRating o2) {
													Double a1 = o1.getRating();
													Double a2 = o2.getRating();
													return a1.compareTo(a2);
												}
											});
											VOPeliculaRating funcion = menor.get(0);
											heap.add(new PC(funcion.getRating()+2, f.llave, a.llave, funcion.getRating(), funcion.getIdPelicula()));
											t=true;
											break re;
										}
									}
								}
							}	
							for(PC pc:aux){
								heap.add(pc);
							}
							if(!t){
								vertices.darValor(a.llave).marca=true;
							}				
						}
					}
					desmarcar();
				}
			}
		}
		
		MergeSort.sort(resp, new Comparator<Lista<PC>>() {
			public int compare(Lista<PC> o1, Lista<PC> o2) {
				return o2.size()-o1.size();
			}
		});
		
		for (Lista<PC> lista : resp) {
			for (PC pc : lista) {
				System.out.print(pc.llave+" | "+pc.idPelicula+" | "+pc.inicio+" --- ");
			}
			System.out.println();
		}
		return resp.get(0);
	}

	
	
	
	class Arco{
		String salida;
		String llegada;
		double peso;
		public Arco(String salida, String llegada, double peso) {
			this.salida = salida;
			this.llegada = llegada;
			this.peso = peso;
		}
		
	}
	
	class Vertice{
		String llave;
		VOTeatro valor;
		EncadenamientoSeparadoTH<String, Arco>  relaciones;
		boolean marca;
		
		public Vertice(String llave, VOTeatro valor, EncadenamientoSeparadoTH<String,Arco> relaciones) {
			this.llave = llave;
			this.valor = valor;
			this.relaciones = relaciones;
			marca = false;
		}

		public void dfs(Lista<NodoCamino<String>> keys, String padre, double peso, int arcos){		
			if(!marca){
				marca=true;
				keys.add(new NodoCamino<String>(llave, padre, peso, arcos));	
				for (Arco a : relaciones.valores()) {
					vertices.darValor(a.llegada).dfs(keys, llave, peso+a.peso,arcos+1);
				}					
			}
		}
		
		public void dfs1(Lista<VOPeliculaPlan> keys, String padre, double peso, int arcos, Lista<Integer> nv, double distancia, int dia){		
			if(!marca){
				if(peso>=8&&peso<12){
					distancia = distancia+(distancia*0.15);
				}
				else if(peso>=12&&peso<14){
					distancia = distancia+(distancia*0.20);
				}
				Lista<VOPeliculaRating> menor = new Lista<>();
				for(Integer i: nv){
					try{
						for(double h:valor.getCartelera().darValor(dia).darValor(i).getHorarios()){
							if(h>(peso+distancia)){
								menor.add(new VOPeliculaRating(i, h));
							}
						}	
					}catch(Exception e){
						continue;
					}
				}
				if(!menor.isEmpty()){
					MergeSort.sort(menor, new Comparator<VOPeliculaRating>() {
						public int compare(VOPeliculaRating o1, VOPeliculaRating o2) {
							Double a1 = o1.getRating();
							Double a2 = o2.getRating();
							return a1.compareTo(a2);
						}
					});
					VOPeliculaRating f = menor.get(0);
					nv.remove(nv.indexOf(f.getIdPelicula()));
					marca=true;
					keys.add(new VOPeliculaPlan(f.getIdPelicula(), valor.getNombre(), f.getRating(), f.getRating()+2, dia, distancia, padre));	
					for (Arco a : relaciones.valores()) {
						vertices.darValor(a.llegada).dfs1(keys, llave, f.getRating()+2, arcos+1, nv, a.peso, dia);
					}
				}
			}
		}
		public void dfs2(Lista<VOPeliculaPlan> keys, String padre, double peso, int arcos, Lista<Integer> nv, double distancia, int dia, String franquicia, String franja){		
			if(!marca&&valor.getFranquicia().getNombre().equals(franquicia)){
				if(peso>=8&&peso<12){
					distancia = distancia+(distancia*0.15);
				}
				else if(peso>=12&&peso<14){
					distancia = distancia+(distancia*0.20);
				}
				Lista<VOPeliculaRating> menor = new Lista<>();
				for(Integer i: nv){
					try{
						for(double h:valor.getCartelera().darValor(dia).darValor(i).getHorarios()){
							if(h>(peso+distancia)){
								if(franja.equals("am")&&h<=12){
									menor.add(new VOPeliculaRating(i, h));
								}
								else if(franja.equals("pm")&&h>=12){
									menor.add(new VOPeliculaRating(i, h));
								}
								else if(franja.equals("")){
									menor.add(new VOPeliculaRating(i, h));
								}
							}
						}	
					}catch(Exception e){
						continue;
					}
				}
				if(!menor.isEmpty()){
					MergeSort.sort(menor, new Comparator<VOPeliculaRating>() {
						public int compare(VOPeliculaRating o1, VOPeliculaRating o2) {
							Double a1 = o1.getRating();
							Double a2 = o2.getRating();
							return a1.compareTo(a2);
						}
					});
					VOPeliculaRating f = menor.get(0);
					nv.remove(nv.indexOf(f.getIdPelicula()));
					marca=true;
					keys.add(new VOPeliculaPlan(f.getIdPelicula(), valor.getNombre(), f.getRating(), f.getRating()+2, dia, distancia, padre));	
					for (Arco a : relaciones.valores()) {
						vertices.darValor(a.llegada).dfs2(keys, llave, f.getRating()+2, arcos+1, nv, a.peso, dia, franquicia, franja);
					}
				}
			}
		}
	}
	
	EncadenamientoSeparadoTH<String, Vertice> vertices;
	Lista<Arco> arcos;
	
	public GrafoTeatros() {
		vertices = new EncadenamientoSeparadoTH<>(5);
		arcos = new Lista<>();
	}
	
	public int numVertices(){
		return vertices.darTamanio();
	}
	
	public int numArcos(){
		return arcos.size();
	}
	
	public VOTeatro darVertice(String id){
		return vertices.darValor(id).valor;
	}
	
	public void agregarVertice(String id, VOTeatro val){
		vertices.insertar(id, new Vertice(id, val, new EncadenamientoSeparadoTH<>(1)));
	}
	
	public void agregarArco(String idOrigen, String idDestino, double peso){
		if(vertices.tieneLlave(idOrigen)&&vertices.tieneLlave(idDestino)){
			Arco a = new Arco(idOrigen, idDestino, peso);
			vertices.darValor(idOrigen).relaciones.insertar(idDestino, a);
			arcos.add(a);
		}
	}
	
	public double darPesoArco(String idOrigen, String idDestino){
		for (Arco arco : arcos) {
			if(arco.salida==idOrigen&&arco.salida==idDestino){
				return arco.peso;
			}
		}
		throw new NoSuchElementException();
	}
	
	public Iterator<String> darVertices(){
		return vertices.llaves().iterator();
	}
	
	public int darGrado(String id){
		if(vertices.tieneLlave(id)){
			return vertices.darValor(id).relaciones.darTamanio();
		}
		throw new NoSuchElementException();
	}
	
	public Iterator<String> darVerticesAdyacentes(String id){
		if(vertices.tieneLlave(id)){
			vertices.darValor(id).relaciones.llaves().iterator();
		}
		throw new NoSuchElementException();
	}
	
	public void desmarcar(){
		for (Vertice ver : vertices.valores()) {
			ver.marca=false;
		}
	}

	public NodoCamino<String>[] DFS(String idOrigen){
		if(vertices.tieneLlave(idOrigen)){
			Lista<NodoCamino<String>> keys = new Lista<>();
			vertices.darValor(idOrigen).dfs(keys, null, 0, 0);
			NodoCamino<String>[] dfs = (NodoCamino<String>[]) new NodoCamino[keys.size()];
			for (int i = 0; i < dfs.length; i++) {
				dfs[i]=keys.get(i);
			}
			desmarcar();
			return dfs;
		}
		throw new NoSuchElementException();
	}
	
	public Lista<VOPeliculaPlan> DFS1(String idOrigen, Lista<Integer> nv, int dia){
			Lista<VOPeliculaPlan> keys = new Lista<>();
			vertices.darValor(idOrigen).dfs1(keys, null, 0, 0, nv.clone(), 0, dia);
			desmarcar();
			return keys;
	}
	public Lista<VOPeliculaPlan> DFS2(String idOrigen, Lista<Integer> nv, int dia, String franquicia, String franja){
		Lista<VOPeliculaPlan> keys = new Lista<>();
		vertices.darValor(idOrigen).dfs2(keys, null, 0, 0, nv.clone(), 0, dia, franquicia, franja);
		desmarcar();
		return keys;
}
	

	public NodoCamino<String>[] BFS(String idOrigen){
		if(vertices.tieneLlave(idOrigen)){
			Lista<NodoCamino<String>> keys = new Lista<>();
			Lista<NodoCamino<String>> i = new Lista<>();
			i.add(new NodoCamino<String>(idOrigen, null, 0, 0));
			while(!i.isEmpty()){
				NodoCamino<String> a = i.remove(0);
				Vertice b = vertices.darValor(a.getVertice());
				keys.add(a);
				b.marca = true;
				for(Arco k: b.relaciones.valores()){
					if(!vertices.darValor(k.llegada).marca){
						i.add(new NodoCamino<String>(k.llegada, b.llave, a.getPeso()+k.peso, a.getArcos()+1));
						vertices.darValor(k.llegada).marca=true;
					}
				}
			}
			NodoCamino<String>[] dfs = (NodoCamino<String>[]) new NodoCamino[keys.size()];
			for (int m = 0; m < dfs.length; m++) {
				dfs[m]=keys.get(m);
			}
			desmarcar();
			return dfs;
		}
		throw new NoSuchElementException();
	}
	
	public NodoCamino<String>[] darCaminoDFS(String idOrigen, String idDestino){
		NodoCamino<String>[] a = DFS(idOrigen);
		Lista<NodoCamino<String>> b = new Lista<>();
		for (NodoCamino<String> nodoCamino : a) {
			if(nodoCamino.getVertice().equals(idDestino)){
				b.add(nodoCamino);
				String padre = nodoCamino.getAnterior();
				while(padre!=null){
					for (NodoCamino<String> nodoCamino1 : a) {
						if (nodoCamino1.getVertice().equals(padre)) {
							b.add(nodoCamino1);
							padre=nodoCamino1.getAnterior();
							break;
						}
					}
				}
				NodoCamino<String>[] dfs = (NodoCamino<String>[]) new NodoCamino[b.size()];
				for (int m = 0; m < dfs.length; m++) {
					dfs[m]=b.get((b.size()-1)-m);
				}
				return dfs;
			}
		}
		return null;
	}

	public Lista<VOPeliculaPlan> darCaminoDFS1(String idOrigen, String idDestino, Lista<Integer> reco, int dia){
		Lista<VOPeliculaPlan> a = DFS1(idOrigen, reco, dia);
		Lista<VOPeliculaPlan> b = new Lista<>();
		for (VOPeliculaPlan nodoCamino : a) {
			if(nodoCamino.getTeatro().equals(idDestino)){
				b.add(nodoCamino);
				String padre = nodoCamino.getAnterior();
				while(padre!=null){
					for (VOPeliculaPlan nodoCamino1 : a) {
						if (nodoCamino1.getTeatro().equals(padre)) {
							b.add(nodoCamino1);
							padre=nodoCamino1.getAnterior();
							break;
						}
					}
				}
				return b;
			}
		}
		return null;
	}
	
	public Lista<VOPeliculaPlan> darCaminoDFS2(String idOrigen, String idDestino, Lista<Integer> reco, int dia, String franquicia, String franja){
		Lista<VOPeliculaPlan> a = DFS2(idOrigen, reco, dia, franquicia, franja);
		Lista<VOPeliculaPlan> b = new Lista<>();
		for (VOPeliculaPlan nodoCamino : a) {
			if(nodoCamino.getTeatro().equals(idDestino)){
				b.add(nodoCamino);
				String padre = nodoCamino.getAnterior();
				while(padre!=null){
					for (VOPeliculaPlan nodoCamino1 : a) {
						if (nodoCamino1.getTeatro().equals(padre)) {
							b.add(nodoCamino1);
							padre=nodoCamino1.getAnterior();
							break;
						}
					}
				}
				return b;
			}
		}
		return null;
	}
	
	public Lista<VOPeliculaPlan> req4(Lista<Integer> reco, int dia){
		Lista<Lista<VOPeliculaPlan>> r = new Lista<>();
		for (String s : vertices.llaves()) {
			for (String s1 : vertices.llaves()) {
				if(!s1.equals(s)){
					r.add(darCaminoDFS1(s, s1, reco, dia));
				}
			}
		}
		Lista<VOPeliculaPlan> f = new Lista<>();
		for (Lista<VOPeliculaPlan> lista : r) {
			if(lista!=null&&lista.size()>f.size()){
				f=lista;
			}
		}
		Lista<VOPeliculaPlan> g = new Lista<>();
		for (int i = 0; i < f.size(); i++) {
			g.add(f.get(f.size()-1-i));
		}
		
		return f;
	}
	
	public Lista<VOPeliculaPlan> req6(Lista<Integer> reco, int dia, String franquicia, String franja){
		Lista<Lista<VOPeliculaPlan>> r = new Lista<>();
		for (String s : vertices.llaves()) {
			for (String s1 : vertices.llaves()) {
				if(!s1.equals(s)){
					r.add(darCaminoDFS2(s, s1, reco, dia, franquicia, franja));
				}
			}
		}
		Lista<VOPeliculaPlan> f = new Lista<>();
		for (Lista<VOPeliculaPlan> lista : r) {
			if(lista!=null&&lista.size()>f.size()){
				f=lista;
			}
		}
		Lista<VOPeliculaPlan> g = new Lista<>();
		for (int i = 0; i < f.size(); i++) {
			g.add(f.get(f.size()-1-i));
		}
		
		return f;
	}
	
	
	
	
	public NodoCamino<String>[] darCaminoBFS(String idOrigen, String idDestino){
		NodoCamino<String>[] a = BFS(idOrigen);
		Lista<NodoCamino<String>> b = new Lista<>();
		for (NodoCamino<String> nodoCamino : a) {
			if(nodoCamino.getVertice().equals(idDestino)){
				b.add(nodoCamino);
				String padre = nodoCamino.getAnterior();
				while(padre!=null){
					for (NodoCamino<String> nodoCamino1 : a) {
						if (nodoCamino1.getVertice().equals(padre)) {
							b.add(nodoCamino1);
							padre=nodoCamino1.getAnterior();
							break;
						}
					}
				}
				NodoCamino<String>[] dfs = (NodoCamino<String>[]) new NodoCamino[b.size()];
				for (int m = 0; m < dfs.length; m++) {
					dfs[m]=b.get((b.size()-1)-m);
				}
				return dfs;
			}
		}
		return null;
	}
	
	public Lista<Lista<String>> caminosCortosDesde(String key){
		class PC implements Comparable<PC>{
			String llave;
			Double peso;
			String anterior;
			public PC(Double peso, String anterior, String llave) {
				this.peso = peso;
				this.anterior = anterior;
				this.llave = llave;
			}
			public int compareTo(PC o) {
				return (-1)*peso.compareTo(o.peso);
			}	
		}
		EncadenamientoSeparadoTH<String, PC> s = new EncadenamientoSeparadoTH<>(1);
		MaxHeapCP<PC> heap = new MaxHeapCP<>();
		Lista<Lista<String>> respuesta = new Lista<>();
		if(vertices.tieneLlave(key)){
			heap.add(new PC(0.0, null, key));
			while(!heap.isEmpty()){
				PC a = heap.max();
				s.insertar(a.llave, a);
				Vertice b = vertices.darValor(a.llave);
				b.marca = true;
				for(Arco c: b.relaciones.valores()){
					boolean t = true;
					MaxHeapCP<PC> heapAux = new MaxHeapCP<>();
					if(!vertices.darValor(c.llegada).marca){
						while(!heap.isEmpty()){
							PC d = heap.max();
							if(c.llegada==d.llave){
								if(d.peso>(a.peso+c.peso)){
									d.peso=(a.peso+c.peso);
									d.anterior=a.llave;
								}
								t = false;
							}
							heapAux.add(d);
						}
						heap = heapAux;
						if(t){
							heap.add(new PC((a.peso+c.peso), a.llave, c.llegada));
						}
					}
				}	
				Lista<String> res = new Lista<>();
				PC n = a;
				while(n!=null){
					res.add(n.llave);
					if(n.anterior!=null){
						n = s.darValor(n.anterior);
					}
					else{
						break;
					}
				}
				respuesta.add(res);
			}
			desmarcar();
			return respuesta;
		}
		throw new NoSuchElementException();
	}

	
	public Lista<Edge> req9(){
		WeightedDirectedGraph<String, String> wg = new WeightedDirectedGraph<>();
		Lista<Edge> respuesta = new Lista<>();
		MaxHeapCP<Edge> cp= new MaxHeapCP<>();
		for(Arco a : arcos){
			cp.add(new Edge(a.salida, a.llegada, a.peso));
		}
		while(!cp.isEmpty()){
			Edge e = cp.max();
			try{
				wg.darVertice(e.getSalida());
				wg.darVertice(e.getLlegada());
				if(darCaminoDFS(e.getSalida(), e.getLlegada())==null||darCaminoDFS(e.getLlegada(), e.getSalida())==null){
					respuesta.add(e);
					try{
						wg.darVertice(e.getSalida());
					}catch (Exception b) {
						wg.agregarVertice(e.getSalida(), e.getSalida());
					}
					try{
						wg.darVertice(e.getLlegada());
					}catch (Exception b) {
						wg.agregarVertice(e.getLlegada(), e.getLlegada());
					}
					wg.agregarArco(e.getSalida(), e.getLlegada(), 0);
				}
			}
			catch(Exception u){
				respuesta.add(e);
				try{
					wg.darVertice(e.getSalida());
				}catch (Exception b) {
					wg.agregarVertice(e.getSalida(), e.getSalida());
				}
				try{
					wg.darVertice(e.getLlegada());
				}catch (Exception b) {
					wg.agregarVertice(e.getLlegada(), e.getLlegada());
				}
				wg.agregarArco(e.getSalida(), e.getLlegada(), 0);
			}
		}
		return respuesta;
	}
	
	public Lista<Lista<String>> req10(String origen, int n){
		Lista<NodoCamino<String>[]> r = new Lista<>();
		
			for (String s1 : vertices.llaves()) {
				if(!s1.equals(origen)){
					r.add(darCaminoDFS(origen, s1));
					r.add(darCaminoBFS(origen, s1));
				}

		}
		Lista<Lista<String>> a = new Lista<>();
		for (NodoCamino<String>[] lista : r) {
			if(lista!=null&&lista.length<=n){
				Lista<String> c = new Lista<>();
				for (NodoCamino<String> string : lista) {
					c.add(string.getVertice());
				}
				a.add(c);
			}
		}
		return a;
	}
}
