package estructuras;

import java.util.Arrays;
import java.util.Iterator;

public class MaxHeapCP<T extends Comparable<T>>{
	
	private T[] heap;
	private int length;
	
	public MaxHeapCP()
	{
		heap = (T[]) new Comparable[100];
		length = 0;
	}
	
	public void add(T elem)
	{
		if (this.length >= heap.length - 1)
		{
			heap = agrandar();
		}
		length++;
		heap[length] = elem;
		ordenar();
	}

	private T[] agrandar()
	{
		return Arrays.copyOf(heap, heap.length*2);
	}
	
	public T max()
	{
		if (isEmpty()) return null;

		T r = maximo();

		cambiar(1, length);
		heap[length] = null;
		length--;

		ordenar1();

		return r;
	}
	
	private void ordenar()
	{
		int i = length;

		while (tienePadre(i) && (padre(i).compareTo(heap[i]) < 0))
		{
			cambiar(i, indiceDelPadre(i));
			i = indiceDelPadre(i);
		}	
	}
	
	private void ordenar1()
	{
		int index = 1;
		while (tieneHijoIzquierdo(index))
		{
			int mayor = indiceIzquierdo(index);
			if (tieneHijoDerecho(index) && heap[indiceIzquierdo(index)].compareTo(heap[indiceDerecho(index)]) < 0)
			{
				mayor = indiceDerecho(index);
			}
			if (heap[index].compareTo(heap[mayor]) < 0)
			{
				cambiar(index, mayor);
			}
			else break;

			index = mayor;
		}				
	}
	
	public int size()
	{
		return length;
	}
	
	public T maximo()
	{
		if (isEmpty()) return null;
		return heap[1];
	}
	
	public boolean isEmpty()
	{
		return length == 0;
	}
	
	private boolean tienePadre(int i)
	{
		return i > 1;
	}

	private int indiceIzquierdo(int i)
	{
		return i * 2;
	}

	private int indiceDerecho(int i)
	{
		return i * 2 + 1;
	}

	private boolean tieneHijoIzquierdo(int i)
	{
		return indiceIzquierdo(i) <= length;
	}

	private boolean tieneHijoDerecho(int i)
	{
		return indiceDerecho(i) <= length;
	}

	private int indiceDelPadre(int i)
	{
		return i / 2;
	}
	
	private T padre(int i)
	{
		return heap[indiceDelPadre(i)];
	}
	
	private void cambiar(int index1, int index2)
	{
		T temp = heap[index1];
		heap[index1] = heap[index2];
		heap[index2] = temp;
	}
}
