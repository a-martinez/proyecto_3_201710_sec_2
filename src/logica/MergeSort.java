package logica;

import java.util.Comparator;

import estructuras.Lista;

public class MergeSort {
	public MergeSort(){}

	
	
	public static <T> void sort(Lista<T> a, Comparator<T> comp){       
        sort(0, a.size()-1,a,comp);
    }
     
    public static <T> void sort(int startIndex,int endIndex, Lista<T> a, Comparator<T> comp){
        if(startIndex<endIndex && (endIndex-startIndex)>=1){
            int mid = (endIndex + startIndex)/2;
            sort(startIndex, mid,a, comp);
            sort(mid+1, endIndex,a, comp);        
             
            merge(startIndex,mid,endIndex,a, comp);            
        }       
    }   
     
    public static <T> void merge(int startIndex,int midIndex,int endIndex,Lista<T> a, Comparator<T> comp){
         
        Lista<T> mergedSortedArray = new Lista<T>();
         
        int leftIndex = startIndex;
        int rightIndex = midIndex+1;
         
        while(leftIndex<=midIndex && rightIndex<=endIndex){
            	if(comp.compare(a.get(leftIndex),(a.get(rightIndex)))<=0){
                mergedSortedArray.add(a.get(leftIndex));
                leftIndex++;
            }else{
                mergedSortedArray.add(a.get(rightIndex));
                rightIndex++;
            }
        }       
         
        while(leftIndex<=midIndex){
            mergedSortedArray.add(a.get(leftIndex));
            leftIndex++;
        }
         
        while(rightIndex<=endIndex){
            mergedSortedArray.add(a.get(rightIndex));
            rightIndex++;
        }
         
        int i = 0;
        int j = startIndex;
        while(i<mergedSortedArray.size()){
            a.set(j, mergedSortedArray.get(i++));
            j++;
        }
    }
}
