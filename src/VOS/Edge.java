package VOS;

public class Edge implements Comparable<Edge> {
	private String salida;
	private String llegada;
	private Double peso;
	public String getSalida() {
		return salida;
	}
	public void setSalida(String salida) {
		this.salida = salida;
	}
	public String getLlegada() {
		return llegada;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public void setLlegada(String llegada) {
		this.llegada = llegada;
	}
	public Edge(String salida, String llegada, double peso) {
		this.peso=peso;
		this.salida = salida;
		this.llegada = llegada;
	}
	@Override
	public int compareTo(Edge o) {
		return -peso.compareTo(o.peso);
	}
	
}
