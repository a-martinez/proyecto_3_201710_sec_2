package VOS;



import estructuras.EncadenamientoSeparadoTH;
import estructuras.Lista;

public class VOPelicula implements Cloneable {
	

	public VOPelicula clone() throws CloneNotSupportedException {
		return new VOPelicula(id, nombre, new Lista<>(), generos.clone(), similitudes);
	}
	private int id;
	private String nombre;
	private Lista<Double> horarios;
	private Lista<String> generos;
	private EncadenamientoSeparadoTH<Integer, Double> similitudes;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Lista<Double> getHorarios() {
		return horarios;
	}
	public void setHorarios(Lista<Double> horarios) {
		this.horarios = horarios;
	}
	public Lista<String> getGeneros() {
		return generos;
	}
	public void setGeneros(Lista<String> generos) {
		this.generos = generos;
	}
	public EncadenamientoSeparadoTH<Integer, Double> getSimilitudes() {
		return similitudes;
	}
	public void setSimilitudes(EncadenamientoSeparadoTH<Integer, Double> similitudes) {
		this.similitudes = similitudes;
	}
	public VOPelicula(int id, String nombre, Lista<Double> horarios, Lista<String> generos,
			EncadenamientoSeparadoTH<Integer, Double> similitudes) {
		this.id = id;
		this.nombre = nombre;
		this.horarios = horarios;
		this.generos = generos;
		this.similitudes = similitudes;
	}

	
	
	
}
