
package VOS;

import estructuras.EncadenamientoSeparadoTH;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOTeatro {
	
	/**
	 * Atributo que modela el nombre del teatro
	 */
	
	private String nombre;
	
	/**
	 * Atributo que modela la ubicacion del teatro 
	 */
	
	private VOUbicacion ubicacion;
	
	/**
	 * Atributo que referencia la franquicia
	 */
	
	private VOFranquicia franquicia;
	
	private EncadenamientoSeparadoTH<Integer, EncadenamientoSeparadoTH<Integer, VOPelicula>> cartelera;

	public VOTeatro(String nombre, VOUbicacion ubicacion, VOFranquicia franquicia) {
		this.nombre = nombre;
		this.ubicacion = ubicacion;
		this.franquicia = franquicia;
		cartelera = new EncadenamientoSeparadoTH<>(1);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public VOUbicacion getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(VOUbicacion ubicacion) {
		this.ubicacion = ubicacion;
	}

	public VOFranquicia getFranquicia() {
		return franquicia;
	}

	public void setFranquicia(VOFranquicia franquicia) {
		this.franquicia = franquicia;
	}

	public EncadenamientoSeparadoTH<Integer, EncadenamientoSeparadoTH<Integer, VOPelicula>> getCartelera() {
		return cartelera;
	}

	public void setCartelera(EncadenamientoSeparadoTH<Integer, EncadenamientoSeparadoTH<Integer, VOPelicula>> cartelera) {
		this.cartelera = cartelera;
	}

	
	
	
	
	
	
}
