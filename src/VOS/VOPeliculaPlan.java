package VOS;


/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOPeliculaPlan {
	
	/**
	 * Atributo que apunta a la pelicula que se propone
	 */
	private int pelicula;
	
	
	/**
	 * Atributo que apunta al teatro que se propone
	 */
	private String teatro;
	
	/**
	 * Atributo que modela la hora de inicio de la pelicula
	 */
	private double horaInicio;
	
	/**
	 * Atributo que modela la hora de fin de la pelicula
	 */
	private double horaFin;
	
	/**
	 * Atributo que modela el dia de presentacion de la pelicla
	 * (1..5)
	 */
	
	private int dia;
	
	
	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	public String getAnterior() {
		return anterior;
	}

	public void setAnterior(String anterior) {
		this.anterior = anterior;
	}

	private double distancia;
	private String anterior;
	

	public int getPelicula() {
		return pelicula;
	}

	public void setPelicula(int pelicula) {
		this.pelicula = pelicula;
	}

	public String getTeatro() {
		return teatro;
	}

	public void setTeatro(String teatro) {
		this.teatro = teatro;
	}

	public double getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(double horaInicio) {
		this.horaInicio = horaInicio;
	}



	public VOPeliculaPlan(int pelicula, String teatro, double horaInicio, double horaFin, int dia, double distancia,
			String anterior) {
		super();
		this.pelicula = pelicula;
		this.teatro = teatro;
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
		this.dia = dia;
		this.distancia = distancia;
		this.anterior = anterior;
	}

	public double getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(double horaFin) {
		this.horaFin = horaFin;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}
	
	
	

}
