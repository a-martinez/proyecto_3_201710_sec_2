package VOS;

import estructuras.Lista;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOUsuario {
	private Lista<VOPeliculaRating> ratings;
	private String nombre;
	public VOUsuario(Lista<VOPeliculaRating> ratings, String nombre) {
		this.ratings = ratings;
		this.nombre = nombre;
	}
	public Lista<VOPeliculaRating> getRatings() {
		return ratings;
	}
	public void setRatings(Lista<VOPeliculaRating> ratings) {
		this.ratings = ratings;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	
}
