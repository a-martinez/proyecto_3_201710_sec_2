package VOS;

public class PC implements Comparable<PC>{
	public String llave;
	public Double peso;
	public String anterior;
	public double inicio;
	public int idPelicula;
	public PC(Double peso, String anterior, String llave, double inicio, int idPelicula) {
		this.peso = peso;
		this.anterior = anterior;
		this.llave = llave;
		this.inicio = inicio;
		this.idPelicula = idPelicula;
	}
	public int compareTo(PC o) {
		return (-1)*peso.compareTo(o.peso);
	}	
}
