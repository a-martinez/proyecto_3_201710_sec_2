package VOS;
/**
 *  
 * @Author: Tomas F. Venegas 
 */

public class VOFranquicia {
	
	/**
	 * Atributo que modela el nombre de la franquicia
	 */
	private String nombre;
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public VOFranquicia(String nombre) {
		this.nombre = nombre;
	}
	
	
}
