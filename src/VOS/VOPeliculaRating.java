package VOS;

public class VOPeliculaRating {
	private int idPelicula;
	private double rating;
	public int getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(int idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public VOPeliculaRating(int idPelicula, double rating) {
		this.idPelicula = idPelicula;
		this.rating = rating;
	}
	
}
